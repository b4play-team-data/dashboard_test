import os
import pandas as pd
import streamlit as st
import pymysql.cursors

from dotenv import load_dotenv


load_dotenv(verbose=True)

conn = pymysql.connect(
    host=os.getenv('host'),
    user=os.getenv('user'),
    password=os.getenv('pass'),
    port=int(os.getenv('port')),
    cursorclass=pymysql.cursors.DictCursor,
)

cursor = conn.cursor()
sql = "SELECT * FROM b4play.game_playtimes;"
cursor.execute(sql)
rows = cursor.fetchall()
df = pd.DataFrame(rows)

lists = list(range(10))

test = st.multiselect(
    "test", list(df.index), lists
)

data = df.loc[test]
st.write("### TEST", data.sort_index())


