1. interprete env: python 3.9.12, conda 4.12.0

2. miniforge 설치
    ```   
    > brew install miniforge

    > conda create -n b4play.game_playtimes python=3.9.0
    ```
3. 가상환경으로 이동
    ```
    > cd /Users/yourPCname/miniforge3/envs/b4play.game_playtimes/
    ```
4. 가상환경 활성화
    ```
    > conda activate b4play.game_playtimes
    ```
5. 필요한 패키지 설치
    ```
    > pip install -r requirements.txt
    ```
6. 폴더 최상단에 .env 파일을 생성한다.
   
7. .env 파일을 만들어준다. data db 사용.
    ```
    # Data DB
    host=
    user=
    pass=
    port=
    ```
8. 실행
    ```
    > streamlit run /your/file/route.py
    ```
9. 도움말: https://github.com/streamlit/streamlit
